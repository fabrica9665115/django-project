import django_filters
from django import forms

from book.models import Book

class BookFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(
        field_name='title',
        lookup_expr='icontains',
        widget=forms.TextInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm',
                'placeholder': 'INPUT THE TITLE'
            }
        )
    )
    
    author = django_filters.CharFilter(
        field_name='author',
        lookup_expr='icontains',
        widget=forms.TextInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm',
                'placeholder': 'INPUT THE AUTHOR'
            }
        )
    )
    
    pages = django_filters.NumberFilter(
        lookup_expr='icontains',
        widget=forms.NumberInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm',
                'placeholder': 'INPUT THE PAGES'
            }
        )
    )
    
    price = django_filters.NumberFilter(
        lookup_expr='icontains',
        widget=forms.NumberInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm',
                'placeholder': 'INPUT THE PRICE'
            }
        )
    )
    
    class Meta:
        model = Book
        fields = ['title', 'author', 'pages', 'price']
    


