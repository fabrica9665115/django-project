from django.contrib import admin

from book.models import Book

class BookAdmin(admin.ModelAdmin):
    pass


admin.site.register(Book, BookAdmin)