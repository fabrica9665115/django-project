from uuid import uuid4

from django import forms

from book.models import Book


class BooksForms(forms.ModelForm):
    BOOK_TYPE_CHOICES = (
        ('TERROR', 'TERROS'),
        ('FICÇÃO', 'FICÇÃO'),
        ('AVENTURA', 'AVENTURA'),
        ('COMÉDIA', 'COMÉDIA'),
        ('INFANTIL', 'INFANTIL'),
    )

    car_id = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm hidden',
                'placeholder': 'INPUT THE BRAND',
                'readonly': True
            }
        )
    )

    title = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm',
                'placeholder': 'INPUT THE TITLE'
            }
        )
    )
    
    gender = forms.ChoiceField(
        choices=BOOK_TYPE_CHOICES,
        widget=forms.Select(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm',
                'placeholder': 'INPUT'
            }
        )
    )
    
    author = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm',
                'placeholder': 'INPUT THE AUTHOR'
            }
        )
    )
    
    pages = forms.IntegerField(
        widget=forms.NumberInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm',
                'placeholder': 'INPUT THE PAGES'
            }
        )
    )
    
    price = forms.DecimalField(
        widget=forms.NumberInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm',
                'placeholder': 'INPUT THE PRICE'
            }
        )
    ) 
    
    
    photo_book = forms.ImageField()

    class Meta:
        model = Book
        fields = '__all__'

    def __init__(self, detail: bool = False, *args, **kwargs):
        super(BooksForms, self).__init__(*args, **kwargs)

        if detail:
            for _, field in self.fields.items():
                field.widget.attrs["disabled"] = True