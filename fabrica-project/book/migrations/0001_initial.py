from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('book_id', models.UUIDField(auto_created=True, default='34bcdc53-acb8-42fb-af47-2d3b3147f1d5', primary_key=True, serialize=False)),                              
                ('title', models.CharField(max_length=100)),
                ('gender', models.CharField(choices=[('Terror'), ('Aventura'),('Ficção'),('Comédia'), ('Infantil')], max_length=10)),
                ('author', models.CharField(max_length=100)),
                ('pages', models.IntegerField()),
                ('price', models.DecimalField(default=0,decimal_places=2, max_digits=4))
                ('photo_book', models.ImageField(upload_to='photos/')),
            ],
        ),
    ]
