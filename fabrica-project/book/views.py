from uuid import uuid4

from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views.generic import View

from book.forms import BooksForms
from book.filters import BookFilter
from book.models import Book

# FALTA IMPLEMENTAR A VIEW DE EDIÇÃO
class BooksListView(View):
    def get(self, request):
        filter_form = BookFilter(request.GET, request=request)
        
        return render(request,'book/list.html', {
            'filter_form': {
                'title': "Books List"
            },
            "context": {
                "author": request.GET.get('author'),
                "pages": request.GET.get('pages'),
                "filter_form": filter_form                
            }
        })
    
# FALTA IMPLEMENTAR A VIEW DE CRIAÇÃO
class BooksCreateView(View):
    def post(self, request):
        create_form = BooksForms(request.POST, request.FILES)
        if create_form.is_valid():
            create_form.save()
            return redirect(
                f"{reverse('books-list')}?situation={request.POST.get('situation')}"
            )
        
        return render(request, "book/form.html",{
            "extraHeadContext":{
                "title": "Create Book"
            },
            "context": {
                "situation": request.GET.get("situation"),
                "titleOfPage": request.GET.get("situation", "").upper(),
                "form": create_form
            }
        })
    
    def get(self, request):
        create_form = BooksForms(
            initial={
                "car_id": str(uuid4())
            }
        )
        return render(request, "book/form.html", {
            "extraHeadContext": {
                "title": "Create Book"
            },
            "context": {
                "situation": request.GET.get("situation"),
                "titleOfPage": request.GET.get("situation", "").upper(),
                "form": create_form
            }
        })
        
class BooksDeleteView(View):
    def get(self, request, book_id):
        book = Book.objects.filter(book_id=book_id)
        book.delete()
        
        return redirect(
            f"{reverse('books-list')}?situation={request.GET.get('situation')}"
        )
    
# FAZER A IMPLEMENTAÇÃO DA VIEW DE DETALHE
class BooksDetailView(View):
    def get(self, request, book_id):
        book = get_object_or_404(Book, book_id=book_id)
        # detail_form = BooksForms(detail=True, initial=book.to_dict())
        # return render(request, "book/form.html", {
        #     "extraHeadContext": {
        #         "title": "Book Detail"
        #     },
        #     "context": {
        #         "situation": request.GET.get("situation"),
        #         "titleOfPage": book.situation.upper(),
        #         "mode": 'detail',
        #         "form": detail_form
        #     }
        # })
        return render(request, 'book/detail.html', {'livro': book})
