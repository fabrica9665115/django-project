from django.db import models


class Book(models.Model):
    BOOK_TYPE_CHOICES = (
        ('TERROR', 'TERROS'),
        ('FICÇÃO', 'FICÇÃO'),
        ('AVENTURA', 'AVENTURA'),
        ('COMÉDIA', 'COMÉDIA'),
        ('INFANTIL', 'INFANTIL'),
    )

    book_id = models.UUIDField(primary_key=True, null=False, blank=False, auto_created=True)
    title = models.CharField(max_length=100),
    gender = models.CharField(max_length=10, choices=BOOK_TYPE_CHOICES),
    author = models.CharField(max_length=100),
    pages = models.IntegerField(),
    price = models.DecimalField(max_digits=4, decimal_places=2, default=0)
    photo_book = models.ImageField(upload_to='photos/')

    def to_dict(self):
        return {
            "book_id": str(self.book_id),
            "title": self.title,
            "gender": self.gender,
            "author": self.author,
            "pages": self.pages,
            "price": self.price,
            "photo_book": self.photo_book,
        }