from django.urls import path, re_path
from book.views import BooksListView, BooksCreateView, BooksDeleteView, BooksDetailView

urlpatterns = [
    path(r'', BooksListView.as_view(), name='books_list'),
    path('register/', BooksCreateView.as_view(),name='books-register'),
    path('<str:book_id>/', BooksDeleteView.as_view(), name='books-delete'),
    path('<str:car_id>/detail', BooksDetailView.as_view(), name='books-detail')
]