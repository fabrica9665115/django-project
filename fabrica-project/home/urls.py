from django.urls import path

from home.views import HomeView
from .views import BaseView

urlpatterns = [
    path('', HomeView.as_view(), name='base'),
    path('home/', BaseView.as_view(), name='home'),
]
