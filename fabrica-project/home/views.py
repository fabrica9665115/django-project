from django.shortcuts import render
from django.views.generic import View

class HomeView(View):
    def get(self, request):
        return render(
        request, 
       'base.html',
        {
            "extra_context":{
                "title":"Base",
            }
        }
    )

class BaseView(View):
    def get(self, request):
        return render(
        request, 
       'home.html',
        {
            "extra_context":{
                "title":"Home",
            }
        }
    )

